package ph.com.easimed.healthcheck;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ph.com.easimed.healthcheck.core.HealthCheckExecutor;
import ph.com.easimed.healthcheck.mail.MailQueueRunner;
import ph.com.easimed.healthcheck.redis.RedisSubscriber;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class Main {
    public static void main(String args[]) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

        MailQueueRunner mailQueueRunner = context.getBean("mailQueueRunner", MailQueueRunner.class);
        HealthCheckExecutor healthCheckExecutor = context.getBean("healthCheckExecutor", HealthCheckExecutor.class);
        RedisSubscriber configsRedisSubscriber = context.getBean("configsRedisSubscriber", RedisSubscriber.class);
        String configs = context.getBean("configs", String.class);

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleWithFixedDelay(mailQueueRunner, 0, 1000, TimeUnit.MILLISECONDS);

        healthCheckExecutor.execute();
        configsRedisSubscriber.subscribe(configs);
    }
}
