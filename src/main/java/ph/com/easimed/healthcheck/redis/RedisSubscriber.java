package ph.com.easimed.healthcheck.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class RedisSubscriber {
    private Jedis jedis;
    private JedisPubSub jedisPubSub;

    public RedisSubscriber(Jedis jedis, JedisPubSub jedisPubSub) {
        this.jedis = jedis;
        this.jedisPubSub = jedisPubSub;
    }

    public void subscribe(final String channel) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                jedis.subscribe(jedisPubSub, channel);
                jedis.quit();
            }
        }, channel + "SubscriberThread").start();
    }
}
