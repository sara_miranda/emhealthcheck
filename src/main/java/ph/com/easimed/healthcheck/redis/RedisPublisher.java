package ph.com.easimed.healthcheck.redis;

import redis.clients.jedis.Jedis;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class RedisPublisher {
    private Jedis jedis;

    public RedisPublisher(Jedis jedis) {
        this.jedis = jedis;
    }

    public void publish(final String channel, final String message) {
        jedis.publish(channel, message);
    }
}
