package ph.com.easimed.healthcheck.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ph.com.easimed.healthcheck.domain.HealthCheckStatus;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class RedisResultWriter {
    private Jedis jedis;
    private ObjectMapper mapper;
    private String results;

    public RedisResultWriter(Jedis jedis, ObjectMapper mapper, String results) {
        this.jedis = jedis;
        this.mapper = mapper;
        this.results = results;
    }

    public void setResults(List<HealthCheckStatus> healthCheckStatuses) {
        try {
            String value = mapper.writeValueAsString(healthCheckStatuses);
            System.out.println("-----------" + value);
            jedis.set(results, value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
