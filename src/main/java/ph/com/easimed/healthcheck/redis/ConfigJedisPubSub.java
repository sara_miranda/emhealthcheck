package ph.com.easimed.healthcheck.redis;

import ph.com.easimed.healthcheck.core.HealthCheckExecutor;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class ConfigJedisPubSub extends JedisPubSub {
    private HealthCheckExecutor healthCheckExecutor;

    public ConfigJedisPubSub(HealthCheckExecutor healthCheckExecutor) {
        this.healthCheckExecutor = healthCheckExecutor;
    }

    @Override
    public void onMessage(String channel, String message) {
        healthCheckExecutor.execute();
    }
}
