package ph.com.easimed.healthcheck.core;

import ph.com.easimed.healthcheck.domain.HealthCheckSetting;
import ph.com.easimed.healthcheck.redis.RedisConfigReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class HealthCheckExecutor {
    private RedisConfigReader redisConfigReader;
    private HealthChecker healthChecker;
    private HealthCheckStatusPublisher healthCheckStatusPublisher;
    private List<Thread> threadPool;

    public HealthCheckExecutor(RedisConfigReader redisConfigReader, HealthChecker healthChecker, HealthCheckStatusPublisher healthCheckStatusPublisher) {
        this.redisConfigReader = redisConfigReader;
        this.healthChecker = healthChecker;
        this.healthCheckStatusPublisher = healthCheckStatusPublisher;
        threadPool = new ArrayList<>();
    }

    public void execute() {
        refreshThreads();

        List<HealthCheckSetting> healthCheckSettings = redisConfigReader.getConfigs();
        System.out.println("HealthCheckSetting size : " + healthCheckSettings.size());
        for (HealthCheckSetting healthCheckSetting : healthCheckSettings) {
            startHealthCheckThread(healthCheckSetting);
        }
    }

    private void refreshThreads() {
        for (Thread thread : threadPool) {
            thread.interrupt();

            System.out.println("Tnterrupting a thread");
        }

        threadPool = new ArrayList<>();
    }

    private void startHealthCheckThread(HealthCheckSetting healthCheckSetting) {
        HealthCheckRunner healthCheckRunner = new HealthCheckRunner(healthChecker, healthCheckSetting, healthCheckStatusPublisher);
        Thread thread = new Thread(healthCheckRunner);
        thread.start();

        threadPool.add(thread);

        System.out.println("Thread Pool Size : " + threadPool.size());
    }
}
