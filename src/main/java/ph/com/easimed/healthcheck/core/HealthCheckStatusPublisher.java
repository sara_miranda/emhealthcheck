package ph.com.easimed.healthcheck.core;

import ph.com.easimed.healthcheck.domain.HealthCheckEmailReceiver;
import ph.com.easimed.healthcheck.domain.HealthCheckSetting;
import ph.com.easimed.healthcheck.domain.HealthCheckStatus;
import ph.com.easimed.healthcheck.mail.Mail;
import ph.com.easimed.healthcheck.redis.RedisPublisher;
import ph.com.easimed.healthcheck.redis.RedisResultWriter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class HealthCheckStatusPublisher {
    private Map<Integer, HealthCheckSetting> healthCheckSettingMap;
    private Map<Integer, HealthCheckStatus> healthCheckStatusMap;
    private RedisResultWriter redisResultWriter;
    private RedisPublisher redisPublisher;
    private String results;
    private Queue<Mail> mailQueue;
    private String serverUpTitle;
    private String serverUpMessage;
    private String serverDownTitle;
    private String serverDownMessage;

    public HealthCheckStatusPublisher(Map<Integer, HealthCheckSetting> healthCheckSettingMap, Map<Integer, HealthCheckStatus> healthCheckStatusMap, RedisResultWriter redisResultWriter, RedisPublisher redisPublisher, String results, Queue<Mail> mailQueue, String serverUpTitle, String serverUpMessage, String serverDownTitle, String serverDownMessage) {
        this.healthCheckSettingMap = healthCheckSettingMap;
        this.healthCheckStatusMap = healthCheckStatusMap;
        this.redisResultWriter = redisResultWriter;
        this.redisPublisher = redisPublisher;
        this.results = results;
        this.mailQueue = mailQueue;
        this.serverUpTitle = serverUpTitle;
        this.serverUpMessage = serverUpMessage;
        this.serverDownTitle = serverDownTitle;
        this.serverDownMessage = serverDownMessage;
    }

    public void updateServerStatus(HealthCheckSetting healthCheckSetting, boolean isHealthy) {
        System.out.println(isHealthy + " : " + healthCheckSetting);

        healthCheckSettingMap.put(healthCheckSetting.getHmoId(), healthCheckSetting);

        HealthCheckStatus healthCheckStatus = healthCheckStatusMap.get(healthCheckSetting.getHmoId());
        if (healthCheckStatus == null) {
            healthCheckStatus = new HealthCheckStatus();
            healthCheckStatus.setHmoId(healthCheckSetting.getHmoId());
            healthCheckStatusMap.put(healthCheckSetting.getHmoId(), healthCheckStatus);
        }

        if (isHealthy) {
            healthCheckStatus.setConsecutiveHealthy(healthCheckStatus.getConsecutiveHealthy() + 1);
            healthCheckStatus.setConsecutiveUnhealthy(0);
        } else {
            healthCheckStatus.setConsecutiveHealthy(0);
            healthCheckStatus.setConsecutiveUnhealthy(healthCheckStatus.getConsecutiveUnhealthy() + 1);
        }

        thresholdCheck(healthCheckSetting);
    }

    private void thresholdCheck(HealthCheckSetting healthCheckSetting) {
        HealthCheckStatus healthCheckStatus = healthCheckStatusMap.get(healthCheckSetting.getHmoId());

        if (healthCheckSetting.getHealthyStatusThreshold() == healthCheckStatus.getConsecutiveHealthy()) {
            healthCheckStatus.setDateOfLastStatusChange(new Date());
            healthCheckStatus.setServerStatusGood(true);
            publishChanges();
            notifyEmailSubscribersUptime(healthCheckSetting);
        }

        if (healthCheckSetting.getUnhealthyStatusThreshold() == healthCheckStatus.getConsecutiveUnhealthy()) {
            healthCheckStatus.setDateOfLastStatusChange(new Date());
            healthCheckStatus.setServerStatusGood(false);
            publishChanges();
            notifyEmailSubscribersDowntime(healthCheckSetting);
        }
    }

    private void publishChanges() {
        List<HealthCheckStatus> healthCheckStatuses = new ArrayList<>(healthCheckStatusMap.values());
        redisResultWriter.setResults(healthCheckStatuses);
        redisPublisher.publish(results, "");
    }

    private void notifyEmailSubscribersUptime(HealthCheckSetting healthCheckSetting) {
        List<HealthCheckEmailReceiver> healthCheckEmailReceivers = healthCheckSetting.getHealthCheckEmailReceiver();
        StringBuilder sb = new StringBuilder();

        for(HealthCheckEmailReceiver healthCheckEmailReceiver : healthCheckEmailReceivers) {
            sb.append(healthCheckEmailReceiver.getEmail()).append(",");
        }

        Mail mail = new Mail();
        mail.setSubject(serverUpTitle);
        mail.setMessage(healthCheckSetting.getServerUrl() + " " + serverUpMessage + " " + getPHTime());
        mail.setRecipient(sb.deleteCharAt(sb.length() - 1).toString());
        mailQueue.add(mail);
    }

    private void notifyEmailSubscribersDowntime(HealthCheckSetting healthCheckSetting) {
        List<HealthCheckEmailReceiver> healthCheckEmailReceivers = healthCheckSetting.getHealthCheckEmailReceiver();
        StringBuilder sb = new StringBuilder();

        for(HealthCheckEmailReceiver healthCheckEmailReceiver : healthCheckEmailReceivers) {
            sb.append(healthCheckEmailReceiver.getEmail()).append(",");
        }

        Mail mail = new Mail();
        mail.setSubject(serverDownTitle);
        mail.setMessage(healthCheckSetting.getServerUrl() + " " + serverDownMessage + " " + getPHTime());
        mail.setRecipient(sb.deleteCharAt(sb.length() - 1).toString());
        mailQueue.add(mail);
    }

    private String getPHTime() {
        DateFormat formatter= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
        return formatter.format(new Date());
    }
}
