package ph.com.easimed.healthcheck.core;

import ph.com.easimed.healthcheck.domain.HealthCheckSetting;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class HealthCheckRunner implements Runnable {
    private HealthCheckSetting healthCheckSetting;
    private HealthChecker healthChecker;
    private HealthCheckStatusPublisher healthCheckStatusPublisher;

    public HealthCheckRunner(HealthChecker healthChecker, HealthCheckSetting healthCheckSetting, HealthCheckStatusPublisher healthCheckStatusPublisher) {
        this.healthChecker = healthChecker;
        this.healthCheckSetting = healthCheckSetting;
        this.healthCheckStatusPublisher = healthCheckStatusPublisher;
    }

    @Override
    public void run() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            public void run() {
                healthCheckStatusPublisher.updateServerStatus(healthCheckSetting, healthChecker.isHealthy(healthCheckSetting));
            }
        }, 0, healthCheckSetting.getRequestInterval(), TimeUnit.SECONDS);

        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("InterruptedException: " + e.getLocalizedMessage());
            }
        }

        System.out.println("Thread was interrupted");
        scheduledExecutorService.shutdown();
    }
}
