package ph.com.easimed.healthcheck.core;

import ph.com.easimed.healthcheck.domain.HealthCheckSetting;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class HealthChecker {
    private static final int THOUSAND_MILLISECONDS = 1000;
    private static final int FAILED_RESPONSE_STATUS = 400;
    private static final String GET_METHOD = "GET";

    public boolean isHealthy(HealthCheckSetting healthCheckSetting) {
        String serverUrl = healthCheckSetting.getServerUrl();
        int timeout = healthCheckSetting.getTimeout();

        int responseStatus = getHttpRequestStatus(GET_METHOD, serverUrl, timeout);
        if (responseStatus < FAILED_RESPONSE_STATUS) {
            return true;
        } else {
            return false;
        }
    }

    private int getHttpRequestStatus(String method, String targetURL, int timeout) {
        try {
            URL url = new URL(targetURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setConnectTimeout(timeout * THOUSAND_MILLISECONDS);

            int statusCode = connection.getResponseCode();
            connection.disconnect();

            return statusCode;
        } catch (IOException ie) {
            return FAILED_RESPONSE_STATUS;
        }
    }
}
