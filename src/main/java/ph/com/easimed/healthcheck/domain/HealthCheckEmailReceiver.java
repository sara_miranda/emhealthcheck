package ph.com.easimed.healthcheck.domain;

import java.util.Date;

/**
 * Created by nikkiromero on 24/03/2017.
 */
public class HealthCheckEmailReceiver {
    private int healthCheckEmailReceiverId;
    private int healthCheckSettingId;
    private String email;
    private boolean deleted;
    private String createdBy;
    private String lastModifiedBy;
    private Date dateCreated;
    private Date dateLastModified;

    public int getHealthCheckEmailReceiverId() {
        return healthCheckEmailReceiverId;
    }

    public void setHealthCheckEmailReceiverId(int healthCheckEmailReceiverId) {
        this.healthCheckEmailReceiverId = healthCheckEmailReceiverId;
    }

    public int getHealthCheckSettingId() {
        return healthCheckSettingId;
    }

    public void setHealthCheckSettingId(int healthCheckSettingId) {
        this.healthCheckSettingId = healthCheckSettingId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastModified() {
        return dateLastModified;
    }

    public void setDateLastModified(Date dateLastModified) {
        this.dateLastModified = dateLastModified;
    }

    @Override
    public String toString() {
        return "HealthCheckEmailReceiver{" +
                "healthCheckEmailReceiverId=" + healthCheckEmailReceiverId +
                ", healthCheckSettingId=" + healthCheckSettingId +
                ", email='" + email + '\'' +
                ", deleted=" + deleted +
                ", createdBy='" + createdBy + '\'' +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateLastModified=" + dateLastModified +
                '}';
    }
}
