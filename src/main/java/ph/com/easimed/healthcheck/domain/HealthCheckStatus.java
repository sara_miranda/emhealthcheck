package ph.com.easimed.healthcheck.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class HealthCheckStatus {
    private int hmoId;
    private boolean isServerStatusGood;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Date dateOfLastStatusChange;
    @JsonIgnore
    private int consecutiveHealthy;
    @JsonIgnore
    private int consecutiveUnhealthy;

    public int getHmoId() {
        return hmoId;
    }

    public void setHmoId(int hmoId) {
        this.hmoId = hmoId;
    }

    public boolean isServerStatusGood() {
        return isServerStatusGood;
    }

    public void setServerStatusGood(boolean serverStatusGood) {
        isServerStatusGood = serverStatusGood;
    }

    public Date getDateOfLastStatusChange() {
        return dateOfLastStatusChange;
    }

    public void setDateOfLastStatusChange(Date dateOfLastStatusChange) {
        this.dateOfLastStatusChange = dateOfLastStatusChange;
    }

    public int getConsecutiveHealthy() {
        return consecutiveHealthy;
    }

    public void setConsecutiveHealthy(int consecutiveHealthy) {
        this.consecutiveHealthy = consecutiveHealthy;
    }

    public int getConsecutiveUnhealthy() {
        return consecutiveUnhealthy;
    }

    public void setConsecutiveUnhealthy(int consecutiveUnhealthy) {
        this.consecutiveUnhealthy = consecutiveUnhealthy;
    }

    @Override
    public String toString() {
        return "HealthCheckStatus{" +
                "hmoId=" + hmoId +
                ", isServerStatusGood=" + isServerStatusGood +
                ", dateOfLastStatusChange=" + dateOfLastStatusChange +
                ", consecutiveHealthy=" + consecutiveHealthy +
                ", consecutiveUnhealthy=" + consecutiveUnhealthy +
                '}';
    }
}
