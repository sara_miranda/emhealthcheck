package ph.com.easimed.healthcheck.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by nikkiromero on 07/03/2017.
 */
public class HealthCheckSetting {
    private int healthCheckSettingId;
    private int hmoId;
    private String serverUrl;
    private int requestInterval;
    private int timeout;
    private int healthyStatusThreshold;
    private int unhealthyStatusThreshold;
    private boolean deleted;
    private String createdBy;
    private String lastModifiedBy;

    private Date dateCreated;
    private Date dateLastModified;
    private List<HealthCheckEmailReceiver> healthCheckEmailReceiver;

    public int getHealthCheckSettingId() {
        return healthCheckSettingId;
    }

    public void setHealthCheckSettingId(int healthCheckSettingId) {
        this.healthCheckSettingId = healthCheckSettingId;
    }

    public int getHmoId() {
        return hmoId;
    }

    public void setHmoId(int hmoId) {
        this.hmoId = hmoId;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public int getRequestInterval() {
        return requestInterval;
    }

    public void setRequestInterval(int requestInterval) {
        this.requestInterval = requestInterval;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getHealthyStatusThreshold() {
        return healthyStatusThreshold;
    }

    public void setHealthyStatusThreshold(int healthyStatusThreshold) {
        this.healthyStatusThreshold = healthyStatusThreshold;
    }

    public int getUnhealthyStatusThreshold() {
        return unhealthyStatusThreshold;
    }

    public void setUnhealthyStatusThreshold(int unhealthyStatusThreshold) {
        this.unhealthyStatusThreshold = unhealthyStatusThreshold;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastModified() {
        return dateLastModified;
    }

    public void setDateLastModified(Date dateLastModified) {
        this.dateLastModified = dateLastModified;
    }

    public List<HealthCheckEmailReceiver> getHealthCheckEmailReceiver() {
        return healthCheckEmailReceiver;
    }

    public void setHealthCheckEmailReceiver(List<HealthCheckEmailReceiver> healthCheckEmailReceiver) {
        this.healthCheckEmailReceiver = healthCheckEmailReceiver;
    }

    @Override
    public String toString() {
        return "HealthCheckSetting{" +
                "healthCheckSettingId=" + healthCheckSettingId +
                ", hmoId=" + hmoId +
                ", serverUrl='" + serverUrl + '\'' +
                ", requestInterval=" + requestInterval +
                ", timeout=" + timeout +
                ", healthyStatusThreshold=" + healthyStatusThreshold +
                ", unhealthyStatusThreshold=" + unhealthyStatusThreshold +
                ", deleted=" + deleted +
                ", createdBy='" + createdBy + '\'' +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateLastModified=" + dateLastModified +
                ", healthCheckEmailReceiver=" + healthCheckEmailReceiver +
                '}';
    }
}