package ph.com.easimed.healthcheck.mail;

/**
 * Created by nikkiromero on 27/03/2017.
 */
public class Mail {
    private String recipient;
    private String sender;
    private String subject;
    private String message;
    private String bccRecipient;

    public String getBccRecipient() { return bccRecipient; }

    public void setBccRecipient(String bccRecipient) { this.bccRecipient = bccRecipient; }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "recipient='" + recipient + '\'' +
                ", sender='" + sender + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", bccRecipient='" + bccRecipient + '\'' +
                '}';
    }
}
