package ph.com.easimed.healthcheck.mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

import com.sun.mail.smtp.SMTPAddressFailedException;
/**
 * Created by nikkiromero on 27/03/2017.
 */
public class JavaMailSender {
      private static Properties properties;
//    private String smtpUsername;
//    private String smtpPassword;
//    private String host;
//    private String inquirySender;

    //Code for email fix
    private static JavaMailSender instance;

    private static String awsHost;
    private static String awsUsername;
    private static String awsPassword;
    private static String awsInquirySender;

    private static String googleHost;
    private static String googleUsername;
    private static String googlePassword;
    private static String googleInquirySender;
    private static String bccInquiryReceiver;

/*
    public JavaMailSender(Properties properties, String smtpUsername, String smtpPassword, String host, String inquirySender, ) {
        this.properties = properties;
        this.smtpUsername = smtpUsername;
        this.smtpPassword = smtpPassword;
        this.host = host;
        this.inquirySender = inquirySender;
    }
*/

    public JavaMailSender(Properties properties, String awsUsername, String awsPassword, String awsHost, String awsInquirySender, String googleUsername, String googlePassword, String googleHost, String googleInquirySender, String bccInquiryReceiver) {
        this.properties = properties;
        this.awsUsername = awsUsername;
        this.awsPassword = awsPassword;
        this.awsHost = awsHost;
        this.awsInquirySender = awsInquirySender;
        this.googleUsername = googleUsername;
        this.googlePassword = googlePassword;
        this.googleHost = googleHost;
        this.googleInquirySender = googleInquirySender;
        this.bccInquiryReceiver = bccInquiryReceiver;
    }

    public synchronized static JavaMailSender getInstance() {
        if (instance == null) {
            instance = new JavaMailSender(properties, awsUsername, awsPassword, awsHost, awsInquirySender,  googleUsername,  googlePassword,  googleHost,  googleInquirySender, bccInquiryReceiver);
        }

        return instance;
    }


    public synchronized boolean sendEmail(Mail mail) {
//        LOGGER.info("MCO sendEmail");
//        LOGGER.info("mail : " + mail);

        if (trySendEmail(mail, properties, awsUsername, awsPassword, awsInquirySender, awsHost, bccInquiryReceiver)) {
//            LOGGER.info("Email was sent using AWS. ");
            return true;
       } else if (trySendEmail(mail, properties, googleUsername, googlePassword, googleInquirySender, googleHost, bccInquiryReceiver)) {
//            LOGGER.info("Email was sent using Google. ");
            return true;
        } else {
//            LOGGER.info("Email was not sent. ");
            return false;
        }
    }

    private synchronized boolean trySendEmail(Mail mail, Properties properties, final String username, final String password, String inquirySender, String host, final String bccInquiryReceiver) {

        try {
            Session mailSession = Session.getInstance(properties, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Transport transport = mailSession.getTransport();
            MimeMessage message = new MimeMessage(mailSession);
            message.setSubject(mail.getSubject());
            message.setFrom(new InternetAddress(inquirySender));

            message.setRecipient(Message.RecipientType.TO, new InternetAddress(bccInquiryReceiver));
            message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(mail.getRecipient()));
            String body = mail.getMessage();
            message.setContent(body, "text/html");

            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
//            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            transport.close();

//            LOGGER.info("MCO trySendEmail : Email was sent successfully. ");
            return true;
        } catch (SMTPAddressFailedException e) {
//            LOGGER.info("MCO trySendEmail : Email sending failed.");
//            LOGGER.info("Exception StackTrace : " + e);
            return false;
        } catch (MessagingException e) {
//            LOGGER.info("MCO trySendEmail : Email sending failed. ");
//            LOGGER.info("Exception StackTrace : " + e);
            return false;
        } catch (Exception e) {
//            LOGGER.info("MCO trySendEmail : Email sending failed. ");
//            LOGGER.info("Exception StackTrace : " + e);
            return false;
        }
    }
}


//    public boolean sendEmail(Mail mail) {
//		try {
//            Session session = Session.getInstance(properties, new Authenticator() {
//                protected PasswordAuthentication getPasswordAuthentication() {
//                    return new PasswordAuthentication(smtpUsername, smtpPassword);
//                }
//            });
//
//            Transport transport = session.getTransport();
//            MimeMessage mimeMessage = new MimeMessage(session);
//            mimeMessage.setSubject(mail.getSubject());
//            mimeMessage.setFrom(new InternetAddress(inquirySender));
//            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mail.getRecipient()));
//            mimeMessage.setContent(mail.getMessage(), "text/html");
//
//            transport.connect(host, smtpUsername, smtpPassword);
//            transport.sendMessage(mimeMessage, mimeMessage.getRecipients(Message.RecipientType.TO));
//            transport.close();
//            return true;
//        } catch (MessagingException e) {
//            System.out.println(e);
//            e.printStackTrace();
//            return false;
//        }
//    }
