package ph.com.easimed.healthcheck.mail;

import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Created by nikkiromero on 27/03/2017.
 */
public class MailQueueRunner implements Runnable {
    private static JavaMailSender javaMailSender;
    private static Queue<Mail> mailQueue;

    public MailQueueRunner(JavaMailSender javaMailSender, Queue<Mail> mailQueue) {
        this.javaMailSender = javaMailSender;
        this.mailQueue = mailQueue;
    }

    static {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleWithFixedDelay(new MailQueueRunner(javaMailSender, mailQueue), 0, 1000, TimeUnit.MILLISECONDS);
    }

    @Override
    public void run() {
        if (mailQueue.peek() != null) {
            Mail mail = mailQueue.poll();
            System.out.println("Sending : " + mail);
            boolean isSuccess = javaMailSender.sendEmail(mail);
            if (!isSuccess) {
                mailQueue.add(mail);
                System.out.println("Failed : " + mail);
            } else {
                System.out.println("Success : " + mail);
            }
        }
    }
}
